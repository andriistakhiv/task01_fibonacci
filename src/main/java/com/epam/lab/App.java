package com.epam.lab;

/**
 * @author Andrii Stakhiv
 * Class for the start point.
 */
public class App {
    /**
     * Start point of the program.
     *
     * @param args comand line values
     */
    public static void main(String[] args) {
        OddAndEvenNumbers.showOddAndEvenNumbersInConsole();
        Fibonacci.showFibonacciNumbersWithMinMaxAndPercentage();
    }
}