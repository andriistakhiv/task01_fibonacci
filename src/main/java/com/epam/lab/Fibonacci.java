package com.epam.lab;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Andrii Stakhiv
 * This class contains various methods for manipulating fibonacci numbers.
 */
public class Fibonacci {
    /**
     * Constant 100 %.
     */
    public static final int HUNDRED_PERSENT = 100;
    /**
     * This list storages odd numbers of fibonacci numbers.
     */
    private static List<Long> oddList;
    /**
     * This list storages even numbers of fibonacci numbers.
     */
    private static List<Long> evenList;

    /**
     * This method uses:
     *
     * @see Fibonacci#showMaxOddAndEvenFib()
     * @see Fibonacci#showPercentageOfOddAndEven()
     */
    public static void showFibonacciNumbersWithMinMaxAndPercentage() {
        showMaxOddAndEvenFib();
        showPercentageOfOddAndEven();
    }

    /**
     * This method gives n-fibonacci number that user entered.
     *
     * @param size size of set
     * @return n-fibonacci number
     */
    private static long getFibonacciNumber(final int size) {
        long[] fibonacciArray = new long[size + 1];
        if (size <= 1) {
            return size;
        } else {
            fibonacciArray[0] = 0;
            fibonacciArray[1] = 1;
            for (int i = 2; i <= size; i++) {
                fibonacciArray[i] = fibonacciArray[i - 1] + fibonacciArray[i - 2];
            }
            return fibonacciArray[size];
        }
    }

    /**
     * This method is using for enter n-fibonacci number from user.
     *
     * @return size of set
     */
    private static int getNumOfFibonacciNumbers() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nEnter the number of fibonacci numbers to generate:");
        return scanner.nextInt();
    }

    /**
     * Shows maximum odd and even fibonacci numbers.
     */
    private static void showMaxOddAndEvenFib() {
        int size = getNumOfFibonacciNumbers();
        oddList = new ArrayList<Long>();
        evenList = new ArrayList<Long>();
        for (int i = size; i >= 0; i--) {
            long fibonacciNumber = getFibonacciNumber(i);
            if (fibonacciNumber % 2 == 1) {
                oddList.add(fibonacciNumber);
            } else {
                evenList.add(fibonacciNumber);
            }
        }
        System.out.println("Maximal odd fibonacci number: " + oddList.get(0));
        System.out.println("Maximal even fibonacci number: " + evenList.get(0));
    }

    /**
     * Shows percentage of odd and even fibonacci numbers.
     */
    private static void showPercentageOfOddAndEven() {
        int totalSize = oddList.size() + evenList.size();
        double oddPercentage = (double) oddList.size() / totalSize * HUNDRED_PERSENT;
        double evenPercentage = (double) evenList.size() / totalSize * HUNDRED_PERSENT;
        System.out.printf("Percentage of odd: %.2f %n", oddPercentage);
        System.out.printf("Percentage of even: %.2f", evenPercentage);
    }
}