package com.epam.lab;

import java.util.Scanner;

/**
 * @author Andrii Stakhiv
 * This class contains various methods for input from user and check his interval of numbers.
 */
public class Interval {
    /**
     * Variable for the start of interval.
     */
    private int start;

    /**
     * Variable for the end of interval.
     */
    private int end;

    /**
     * In this method user gives his interval of numbers.
     */
    public void setIntervalFromUser() {
        boolean correctInterval;
        System.out.println("Enter the interval:");
        Scanner scanner = new Scanner(System.in);
        do {
            start = scanner.nextInt();
            end = scanner.nextInt();
            correctInterval = isBGreaterOrEqualToA(start, end);
        } while (correctInterval);

    }

    /**
     * This method checks if user's input interval is correct.
     *
     * @param start start
     * @param end   end
     * @return true if interval is incorrect and false if interval is correct
     */
    private static boolean isBGreaterOrEqualToA(final int start, final int end) {
        if (end > start) {
            return false;
        } else {
            System.out.println("Interval is incorrect, try again");
            return true;
        }
    }

    /**
     * Getter for start.
     *
     * @return start
     */
    public int getStart() {
        return this.start;
    }

    /**
     * Getter for end.
     *
     * @return end
     */
    public int getEnd() {
        return this.end;
    }
}