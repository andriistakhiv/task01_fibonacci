package com.epam.lab;

/**
 * @author Andrii Stakhiv
 * This class contains various methods for manipulating odd and even numbers.
 */
public class OddAndEvenNumbers {
    /**
     * New object for set interval from user.
     */
    private static Interval interval = new Interval();

    /**
     * This method uses:
     *
     * @see Interval#setIntervalFromUser()
     * @see OddAndEvenNumbers#showOddFromStart()
     * @see OddAndEvenNumbers#showEvenFromEnd()
     * @see OddAndEvenNumbers#sumOfOddNumbers()
     * @see OddAndEvenNumbers#sumOfEvenNumbers()
     */
    public static void showOddAndEvenNumbersInConsole() {
        interval.setIntervalFromUser();
        showOddFromStart();
        showEvenFromEnd();
        sumOfOddNumbers();
        sumOfEvenNumbers();
    }

    /**
     * Shows all odd numbers from start to the end.
     */
    private static void showOddFromStart() {
        System.out.print("\nOdd numbers: ");
        for (int i = interval.getStart(); i <= interval.getEnd(); i++) {
            if (i % 2 == 1) {
                System.out.print(i + " ");
            }
        }
    }

    /**
     * Shows all even numbers from end to start.
     */
    private static void showEvenFromEnd() {
        System.out.print("\nEven numbers: ");
        for (int i = interval.getEnd(); i >= interval.getStart(); i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
    }

    /**
     * Shows sum of odd numbers.
     */
    private static void sumOfOddNumbers() {
        int oddSum = 0;
        for (int i = interval.getStart(); i <= interval.getEnd(); i++) {
            if (i % 2 == 1) {
                oddSum += i;
            }
        }
        System.out.print("\nOdd sum: " + oddSum);
    }

    /**
     * Shows sum of even numbers.
     */
    private static void sumOfEvenNumbers() {
        int evenSum = 0;
        for (int i = interval.getEnd(); i >= interval.getStart(); i--) {
            if (i % 2 == 0) {
                evenSum += i;
            }
        }
        System.out.print("\nEven sum: " + evenSum);
    }
}